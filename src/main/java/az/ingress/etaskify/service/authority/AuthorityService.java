package az.ingress.etaskify.service.authority;

import az.ingress.etaskify.dto.request.authority.AuthorityRequestDto;
import az.ingress.etaskify.dto.response.address.AddressResponseDto;
import az.ingress.etaskify.dto.response.authority.AuthorityResponseDto;
import az.ingress.etaskify.model.authority.Authority;
import az.ingress.etaskify.repository.AuthorityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthorityService {
    private final AuthorityRepository authorityRepository;
    private final ModelMapper modelMapper;

    public AuthorityResponseDto create(AuthorityRequestDto requestDto) {
        Optional<Authority> authority = authorityRepository.findByAuthority(requestDto.getAuthority());
        if (authority.isPresent()) {
            throw new RuntimeException("Authority already present");
        }

        Authority authorityForSave = Authority.builder()
                .authority(requestDto.getAuthority())
                .build();
        Authority savedAuthority = authorityRepository.save(authorityForSave);

        return modelMapper.map(savedAuthority, AuthorityResponseDto.class);
    }

    public AuthorityResponseDto get(Long id) {
        Authority authority = authorityRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Authority with id %s not found", id)));
        return modelMapper.map(authority, AuthorityResponseDto.class);
    }

    public List<AuthorityResponseDto> getAll() {
        List<Authority> authorityList = authorityRepository.findAll();
        List<AuthorityResponseDto> authorityResponseDtos = new ArrayList<>();
        authorityList.forEach(authority ->
                authorityResponseDtos.add(modelMapper.map(authority, AuthorityResponseDto.class)));
        return authorityResponseDtos;
    }

    public AuthorityResponseDto update(Long id, AuthorityRequestDto requestDto) {
        Authority authority = authorityRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Authority with id %s not found", id)));

        Authority authorityForUpdate = modelMapper.map(requestDto, Authority.class);
        authorityForUpdate.setId(id);

        return modelMapper.map(authorityRepository.save(authorityForUpdate), AuthorityResponseDto.class);
    }

    public AuthorityResponseDto delete(Long id) {
        Authority authority = authorityRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Authority with id %s not found", id)));

        authorityRepository.deleteById(id);
        return modelMapper.map(authority, AuthorityResponseDto.class);
    }
}
