package az.ingress.etaskify.service.organization;

import az.ingress.etaskify.dto.request.organization.OrganizationRequestDto;
import az.ingress.etaskify.dto.response.organization.OrganizationResponseDto;
import az.ingress.etaskify.model.organization.Organization;
import az.ingress.etaskify.repository.OrganizationRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final ModelMapper modelMapper;

    public OrganizationResponseDto create(OrganizationRequestDto requestDto) {
        Optional<Organization> organization = organizationRepository.findByName(requestDto.getName());
        if (organization.isPresent()) {
            throw new RuntimeException("Organization already present");
        }

        Organization organizationForSave = Organization.builder()
                .name(requestDto.getName())
                .confirmationCode(requestDto.getConfirmationCode())
                .build();
        Organization savedOrganization = organizationRepository.save(organizationForSave);
        return modelMapper.map(savedOrganization, OrganizationResponseDto.class);
    }


    public List<OrganizationResponseDto> getAll() {
        List<Organization> organizationList = organizationRepository.findAll();
        List<OrganizationResponseDto> organizationResponseDtos = new ArrayList<>();
        organizationList.forEach(organization ->
                organizationResponseDtos.add(modelMapper.map(organization, OrganizationResponseDto.class)));
        return organizationResponseDtos;
    }


    public OrganizationResponseDto delete(Long id) {
        Organization organization = organizationRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Organization with id %s not found", id)));

        organizationRepository.deleteById(id);
        return modelMapper.map(organization, OrganizationResponseDto.class);
    }
}
