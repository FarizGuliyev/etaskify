package az.ingress.etaskify.service.user;

import az.ingress.etaskify.dto.request.user.UserCreateRequestDto;
import az.ingress.etaskify.dto.request.user.UserRequestDto;
import az.ingress.etaskify.dto.response.user.UserCreateResponseDto;
import az.ingress.etaskify.dto.response.user.UserResponseDto;
import az.ingress.etaskify.model.address.Address;
import az.ingress.etaskify.model.authority.Authority;
import az.ingress.etaskify.model.authority.UserAuthority;
import az.ingress.etaskify.model.organization.Organization;
import az.ingress.etaskify.model.user.User;
import az.ingress.etaskify.repository.AddressRepository;
import az.ingress.etaskify.repository.AuthorityRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final AddressRepository addressRepository;
    private final JwtService jwtService;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ModelMapper modelMapper;


    public UserCreateResponseDto create(UserCreateRequestDto requestDto) {
        Optional<User> user = userRepository.findUserByEmail(requestDto.getEmail());
        if (user.isPresent()) {
            throw new RuntimeException("User already present");
        }

        Authority userAuthority = authorityRepository.findByAuthority(UserAuthority.USER)
                .orElseThrow(() -> new RuntimeException("Authority not found"));

        if (!requestDto.getPassword().equals(requestDto.getConfirmPassword())) {
            throw new RuntimeException("Passwords didnt match");
        }

        User userForSave = User.builder()
                .authorities(List.of(userAuthority))
                .password(passwordEncoder.encode(requestDto.getPassword()))
                .email(requestDto.getEmail())
                .organization(getOrganizationFromToken(requestDto.getToken()))
                .build();

        userRepository.save(userForSave);

        return UserCreateResponseDto.builder()
                .message(String.format("User registered successfully : %s", userForSave.getEmail()))
                .build();
    }


    public UserResponseDto get(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("User with id %s not found", userId)));
        return modelMapper.map(user, UserResponseDto.class);
    }

    public List<UserResponseDto> getAll(Long id) {
        List<User> userList = userRepository.findAllUsers(id);
        List<UserResponseDto> userResponseDtos = new ArrayList<>();
        userList.forEach(user ->
                userResponseDtos.add(modelMapper.map(user, UserResponseDto.class)));
        return userResponseDtos;
    }


    public UserResponseDto update(Long id, UserRequestDto requestDto) {
        User user = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("User with id %s not found", id)));

        Optional<Address> address = addressRepository.findById(requestDto.getAddressId());

        User userForUpdate = modelMapper.map(requestDto, User.class);
        userForUpdate.setPassword(passwordEncoder.encode(requestDto.getPassword()));
        userForUpdate.setId(id);

        if (address.isPresent()) {
            userForUpdate.getAddress().setId(address.get().getId());
            userForUpdate.getAddress().setAddress(requestDto.getAddress());
        }
        userForUpdate.getAddress().setAddress(requestDto.getAddress());

        requestDto.getPhoneNumbers().forEach(phoneNumber ->
                userForUpdate.getPhoneNumbers().add(phoneNumber));
        requestDto.getPhoneNumbers().forEach(phoneNumber -> phoneNumber.setUser(user));

        User savedUser = userRepository.save(userForUpdate);

        return modelMapper.map(savedUser, UserResponseDto.class);
    }


    public UserResponseDto delete(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RuntimeException(String.format("User with id %s not found", userId)));

        userRepository.deleteById(userId);
        return modelMapper.map(user, UserResponseDto.class);
    }

    public Organization getOrganizationFromToken(String token) {
        String emailFromToken = jwtService.getEmailFromToken(token);
        Optional<User> user = userRepository.findUserByEmail(emailFromToken);
        return user.get().getOrganization();
    }
}
