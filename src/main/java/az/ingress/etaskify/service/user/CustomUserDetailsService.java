package az.ingress.etaskify.service.user;

import az.ingress.etaskify.model.user.User;
import az.ingress.etaskify.security.UserPrincipal;
import az.ingress.etaskify.service.authService.AuthService;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class  CustomUserDetailsService implements UserDetailsService {
    private final AuthService authService;

    @Lazy
    public CustomUserDetailsService(AuthService authService) {
        this.authService = authService;
    }

    public UserDetails loadUserByUsername(String email) {
        User user = authService.findUser(email);

        return UserPrincipal.create(user);
    }
}
