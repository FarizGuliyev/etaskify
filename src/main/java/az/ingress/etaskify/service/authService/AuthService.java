package az.ingress.etaskify.service.authService;

import az.ingress.etaskify.dto.request.auth.AuthLoginRequestDto;
import az.ingress.etaskify.dto.request.auth.AuthRequestDto;
import az.ingress.etaskify.dto.response.auth.AuthResponseDto;
import az.ingress.etaskify.model.authority.Authority;
import az.ingress.etaskify.model.authority.UserAuthority;
import az.ingress.etaskify.model.organization.Organization;
import az.ingress.etaskify.model.user.User;
import az.ingress.etaskify.repository.AuthorityRepository;
import az.ingress.etaskify.repository.OrganizationRepository;
import az.ingress.etaskify.repository.UserRepository;
import az.ingress.etaskify.security.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthService {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;
    private final OrganizationRepository organizationRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    public User findUser(String email) {
        return userRepository.findByEmail(email).orElseThrow(
                () -> new RuntimeException("User not found with given email"));
    }

    public AuthResponseDto register(AuthRequestDto requestDto) {
        Optional<User> user = userRepository.findByEmail(requestDto.getEmail());
        if (user.isPresent()) {
            throw new RuntimeException("Email already present");
        }

        Authority userAuthority = authorityRepository.findByAuthority(UserAuthority.ORGANIZATION_ADMIN)
                .orElseThrow(() -> new RuntimeException("authority not found"));

        if (!requestDto.getPassword().equals(requestDto.getConfirmPassword())) {
            throw new RuntimeException("Passwords didnt match");
        }

        Organization organization = organizationRepository.findByName(requestDto.getOrganizationName()).orElseThrow(
                () -> new RuntimeException("Organization not found")
        );


        if (!organization.getConfirmationCode().equals(requestDto.getConfirmationCode())) {
            throw new RuntimeException("Confirmation code is was wrong");
        }

        User userForSave = User.builder()
                .authorities(List.of(userAuthority))
                .password(passwordEncoder.encode(requestDto.getPassword()))
                .email(requestDto.getEmail())
                .name(requestDto.getName())
                .surname(requestDto.getSurname())
                .organization(organization)
                .build();

        userRepository.save(userForSave);

        return AuthResponseDto.builder()
                .jwtToken(jwtService.issueToken(userForSave))
                .build();
    }

    public AuthResponseDto login(AuthLoginRequestDto requestDto) {
        User user = userRepository.findUserByEmail(requestDto.getEmail()).orElseThrow(() ->
                new RuntimeException(String.format("User not found with email %s", requestDto.getEmail())));

        if (!passwordEncoder.matches(requestDto.getPassword(), user.getPassword())) {
            throw new RuntimeException("Password is wrong");
        }


        return AuthResponseDto.builder()
                .jwtToken(jwtService.issueToken(user))
                .build();
    }
}
