package az.ingress.etaskify.service.phoneNumber;

import az.ingress.etaskify.dto.request.phoneNumber.PhoneNumberRequestDto;
import az.ingress.etaskify.dto.response.phoneNumber.PhoneNumberResponseDto;
import az.ingress.etaskify.model.phoneNumber.PhoneNumber;
import az.ingress.etaskify.repository.PhoneNumberRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class PhoneNumberService {

    private final PhoneNumberRepository phoneNumberRepository;
    private final ModelMapper modelMapper;


    public PhoneNumberResponseDto create(PhoneNumberRequestDto requestDto) {
        PhoneNumber phoneNumber = PhoneNumber.builder()
                .number(requestDto.getNumber())
                .build();
        PhoneNumber savedPhoneNumber = phoneNumberRepository.save(phoneNumber);
        return modelMapper.map(savedPhoneNumber, PhoneNumberResponseDto.class);
    }

    public PhoneNumberResponseDto get(Long id) {
        PhoneNumber phoneNumber = phoneNumberRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Phone Number with id %s not found", id)));
        return modelMapper.map(phoneNumber, PhoneNumberResponseDto.class);
    }

    public List<PhoneNumberResponseDto> getAll() {
        List<PhoneNumber> phoneNumberList = phoneNumberRepository.findAll();
        List<PhoneNumberResponseDto> phoneNumberResponseDtos = new ArrayList<>();
        phoneNumberList.forEach(phoneNumber ->
                phoneNumberResponseDtos.add(modelMapper.map(phoneNumber, PhoneNumberResponseDto.class)));
        return phoneNumberResponseDtos;
    }


    public PhoneNumberResponseDto update(Long id, PhoneNumberRequestDto requestDto) {
        PhoneNumber phoneNumber = phoneNumberRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Phone Number with id %s not found", id)));

        PhoneNumber phoneNumberForUpdate = modelMapper.map(requestDto, PhoneNumber.class);
        phoneNumberForUpdate.setId(id);

        return modelMapper.map(phoneNumberRepository.save(phoneNumberForUpdate), PhoneNumberResponseDto.class);
    }


    public PhoneNumberResponseDto delete(Long id) {
        PhoneNumber phoneNumber = phoneNumberRepository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("Phone Number with id %s not found", id)));

        phoneNumberRepository.deleteById(id);
        return modelMapper.map(phoneNumber, PhoneNumberResponseDto.class);
    }

}
