package az.ingress.etaskify.dto.response.user;

import az.ingress.etaskify.model.address.Address;
import az.ingress.etaskify.model.phoneNumber.PhoneNumber;
import az.ingress.etaskify.model.task.Task;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class UserResponseDto {
    Long id;
    String name;
    String surname;
    List<PhoneNumber> phoneNumbers;
    Address address;
    @Builder.Default
    List<Task> taskList = new ArrayList<>();
}


