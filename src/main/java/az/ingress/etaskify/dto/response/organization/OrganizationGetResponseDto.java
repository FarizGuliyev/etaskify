package az.ingress.etaskify.dto.response.organization;

import az.ingress.etaskify.model.user.User;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class OrganizationGetResponseDto {
    Long id;
    String name;
    String confirmationCode;
    @Builder.Default
    List<User> users = new ArrayList<>();
}
