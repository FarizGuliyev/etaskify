package az.ingress.etaskify.dto.response.authority;

import az.ingress.etaskify.model.authority.UserAuthority;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthorityResponseDto {
    Long id;
    @Enumerated(EnumType.STRING)
    UserAuthority authority;
}
