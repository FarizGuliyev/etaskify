package az.ingress.etaskify.dto.response.phoneNumber;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class PhoneNumberResponseDto {
    Long id;
    String number;
}
