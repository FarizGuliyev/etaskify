package az.ingress.etaskify.dto.request.user;

import az.ingress.etaskify.model.phoneNumber.PhoneNumber;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRequestDto {
    String name;
    String surname;
    String password;
    String confirmPassword;
    Long addressId;
    String address;
    List<PhoneNumber> phoneNumbers;

}
