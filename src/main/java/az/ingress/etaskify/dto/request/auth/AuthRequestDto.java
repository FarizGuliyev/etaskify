package az.ingress.etaskify.dto.request.auth;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AuthRequestDto {
    String name;
    String surname;
    String email;
    String password;
    String confirmPassword;
    String organizationName;
    String confirmationCode;
}
