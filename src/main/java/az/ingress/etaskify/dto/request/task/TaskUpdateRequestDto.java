package az.ingress.etaskify.dto.request.task;

import az.ingress.etaskify.model.task.TaskStatus;
import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskUpdateRequestDto {
    String title;
    String description;
    @JsonFormat(pattern = "dd.MM.yyyy")
    LocalDate deadline;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
}
