package az.ingress.etaskify.model.authority;

public enum UserAuthority {
    ADMIN,
    ORGANIZATION_ADMIN,
    USER
}
