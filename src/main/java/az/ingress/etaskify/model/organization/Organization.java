package az.ingress.etaskify.model.organization;

import az.ingress.etaskify.model.user.User;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "organization")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Organization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String name;
    String confirmationCode;

    @OneToMany(mappedBy = "organization",cascade = CascadeType.REMOVE)
    List<User> users = new ArrayList<>();
}
