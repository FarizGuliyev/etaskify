package az.ingress.etaskify.model.task;

import lombok.Getter;

@Getter
public enum TaskStatus {
    NEW,
    ONGOING,
    DONE,
    SUSPENDED

}
