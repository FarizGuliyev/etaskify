package az.ingress.etaskify.repository;

import az.ingress.etaskify.model.address.Address;
import az.ingress.etaskify.model.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
