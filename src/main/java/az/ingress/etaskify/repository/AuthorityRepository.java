package az.ingress.etaskify.repository;

import az.ingress.etaskify.model.authority.Authority;
import az.ingress.etaskify.model.authority.UserAuthority;
import az.ingress.etaskify.model.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Optional<Authority> findByAuthority(UserAuthority userAuthority);

}
