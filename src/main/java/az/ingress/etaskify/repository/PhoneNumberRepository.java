package az.ingress.etaskify.repository;

import az.ingress.etaskify.model.phoneNumber.PhoneNumber;
import az.ingress.etaskify.model.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber, Long> {
}
