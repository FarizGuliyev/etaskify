package az.ingress.etaskify.controller;

import az.ingress.etaskify.dto.request.address.AddressRequestDto;
import az.ingress.etaskify.dto.response.address.AddressResponseDto;
import az.ingress.etaskify.service.address.AddressService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
@Slf4j
public class AddressController {
    private final AddressService addressService;


    @GetMapping("/{addressId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<AddressResponseDto> get(@PathVariable Long addressId) {
        return ResponseEntity.ok(addressService.get(addressId));
    }

    @GetMapping("/gets")
    @PreAuthorize("hasAuthority('ADMIN')")
    public ResponseEntity<List<AddressResponseDto>> getAll() {
        return ResponseEntity.ok(addressService.getAll());
    }


    @PutMapping("/{addressId}")
    @PreAuthorize("hasAnyAuthority('USER','ORGANIZATION_ADMIN')")
    public ResponseEntity<AddressResponseDto> update(@PathVariable Long addressId, @RequestBody AddressRequestDto requestDto) {
        return ResponseEntity.ok(addressService.update(addressId, requestDto));
    }


    @DeleteMapping("/{addressId}")
    @PreAuthorize("hasAnyAuthority('USER','ORGANIZATION_ADMIN')")
    public ResponseEntity<AddressResponseDto> delete(@PathVariable Long addressId) {
        return ResponseEntity.ok(addressService.delete(addressId));
    }

}
