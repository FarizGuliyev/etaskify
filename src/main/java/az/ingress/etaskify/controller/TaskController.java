package az.ingress.etaskify.controller;

import az.ingress.etaskify.dto.request.task.TaskCreateRequestDto;
import az.ingress.etaskify.dto.request.task.TaskUpdateRequestDto;
import az.ingress.etaskify.dto.request.user.UsersListDto;
import az.ingress.etaskify.dto.response.task.TaskForUsersResponseDto;
import az.ingress.etaskify.dto.response.task.TaskResponseDto;
import az.ingress.etaskify.service.task.TaskService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/task")
@Slf4j
public class TaskController {
    private final TaskService taskService;


    @PostMapping("/new")
    @PreAuthorize("hasAuthority('ORGANIZATION_ADMIN')")
    public ResponseEntity<TaskResponseDto> create(@RequestBody TaskCreateRequestDto dto) {
        return ResponseEntity.ok(taskService.create(dto));
    }


    @GetMapping("/{taskId}")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<TaskResponseDto> get(@PathVariable Long taskId) {
        return ResponseEntity.ok(taskService.get(taskId));
    }


    @GetMapping("/{organizationId}/gets")
    @PreAuthorize("hasAuthority('ORGANIZATION_ADMIN')")
    public ResponseEntity<List<TaskResponseDto>> getAllTasksByOrganizationId(@PathVariable Long organizationId) {
        return ResponseEntity.ok(taskService.getAllTasksByOrganizationId(organizationId));
    }


    @GetMapping("/{userId}/gets")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<List<TaskResponseDto>> getAllTasksByUserId(@PathVariable Long userId) {
        return ResponseEntity.ok(taskService.getAllTasksByUserId(userId));
    }


    @PutMapping("/{taskId}")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN','USER')")
    public ResponseEntity<TaskResponseDto> update(@PathVariable Long taskId, @RequestBody TaskUpdateRequestDto requestDto) {
        return ResponseEntity.ok(taskService.update(taskId, requestDto));
    }


    @PutMapping("/{taskId}/users")
    @PreAuthorize("hasAnyAuthority('ORGANIZATION_ADMIN')")
    public ResponseEntity<TaskForUsersResponseDto> assignUsersToTask(@PathVariable Long taskId,
                                                                     @RequestBody UsersListDto listDto) {

        return ResponseEntity.ok(taskService.assignUsersToTask(taskId, listDto));
    }


    @DeleteMapping("/{taskId}")
    @PreAuthorize("hasAuthority('ORGANIZATION_ADMIN')")
    public ResponseEntity<TaskResponseDto> delete(@PathVariable Long taskId) {
        return ResponseEntity.ok(taskService.delete(taskId));
    }

}
